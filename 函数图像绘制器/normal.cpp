/*normal.cpp新算法
*by TSherry
*2018-8-26
*特别感谢www.docin.com/p-1052761737.html
*孙悦同志在其论文中对编程思想的启发
*/
#include<stdio.h>
#include<iostream>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include"apis.h"

using namespace std;
//命令集
char olders[100][20]={
"add","sub","mul","div","quit",
"ssum","ssub","pow","agr","assi",
"ssqu","umsq","log","ln","value",
"rest"
};

float form,startnumber;//form为参与运算的另一个数字，startnamber为初始数字 
char mathway[10];//运算方法
float result;//运算结果

float cou(float another,char ol[10],float start);
float exdo(float another,int i,float startn);

int main()
{
	cout<<"********************************************"<<endl;
	cout<<"*欢迎使用MasterData 0.04"<<endl;
	cout<<"*请输入初始数字与指令及其参数,注意不要输错命令"<<endl;
	cout<<"部分操作数可能无效"<<endl;
	cout<<"********************************************"<<endl;
	
	cout<<"赋值: \b";
	cin>>startnumber;

	for(;;) //启动计算系统循环 
	{
		cout<<">";//命令提示符
		cin>>mathway>>form;
		result=cou(form,mathway,startnumber);//自动保存上次结果 
		startnumber=result;
		//Save the result.
		//cou(a,b,st);
	}
	return 0;
	
}

float cou(float another,char ol[10],float start)
{
	for(int i=0;i<=100;i++)
	{
	if(strcmp(ol,olders[i])==0)
	{return exdo(another,i,start);}
	else
	{	}
	}
}

float exdo(float another,int i,float startn)
{
	float r;
	double dstartn=(double)(startn);
	double danother=(double)(another);
	switch(i)
	{
	case 0:r=another+startn;cout<<"加法运算 "<<r<<endl;break;
	case 1:r=startn-another;cout<<"减法运算 "<<r<<endl;break;
	case 2:r=another*startn;cout<<"乘法运算 "<<r<<endl;break;
	case 3:r=startn/another;cout<<"除法运算 "<<r<<endl;break;
	case 15:r=(int)(startn)%(int)(another);cout<<"取余"<<r<<endl;break;
	case 4:r=0;cout<<"退出"<<endl;exit(another);break;
	case 5:r=startn*startn+another*another;
		cout<<"平方和运算 "<<r<<endl;break;
	case 6:r=startn*startn-another*another;
		cout<<"平方差运算 "<<r<<endl;break;
	case 7:r=pow(startn,another);
		cout<<"乘方运算 "<<r<<endl;break;
	case 8:r=(another+startn)/2;cout<<"平均数运算 "<<r<<endl;break;
	case 9:r=another;cout<<"赋值 "<<r<<endl;break;
	case 10:r=(another+startn)*(another+startn);
		cout<<"和的平方"<<r<<endl;break;
	case 11:r=(another-startn)*(another-startn);
	case 12:
		r=vlog(dstartn,danother);
		cout<<"对数运算"<<r<<endl;break;
		cout<<"差的平方"<<r<<endl;break;
	case 13:
		cout<<"ln"<<vlog(2.718,dstartn)<<endl;
		cout<<"ln"<<vlog(2.718,danother)<<endl;break;
	case 14:cout<<"查看值\t"<<(r=startn)<<endl;break;
	default:
	r=startn;cout<<"no"<<endl;
	}
	return r;
}

