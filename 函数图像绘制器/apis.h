//#ifdef APIS_H
//#define APIS_H

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>

using namespace std;

//代数学部分
bool ifinteger(float a)
{
	float i;
	for(i=0;i<a;i++)
	{
	a=a-i;
	}
	if(a==0)
	{
		return 1;
	}else
	{
		return 0;
	}
}
//求底数不为10或e的对数
double vlog(double a,double N)
{
	if(a==N){return 1;}
	else if(N==1){return 0;}
	else if(a*a==N){return 2;}
	else if(pow(a,3)==N){return 3;}
	else if(pow(a,4)==N){return 4;}
	else if(pow(a,5)==N){return 5;}
	else{
	double a1=log(a);
	double N1=log(N);
	double a2=log10(a);
	double N2=log10(N);
	/*借助换底公式估算
	求以10和e为底的对数商的平均值*/
	return (N1/a1+N2/a2)/2;
	}
}
//求阶乘
int factial(int n)
{
	if(n==1)
	{
	return 1;
	}
	else
	{
	return n*factial(n-1);
	}
}
//求第n个数字的斐波纳契数列的数字
int fibo(int n)
{
	if(n==1||n==2)
	{
	return 1;
	}
	else
	{
	return fibo(n-1)+fibo(n-2);
	}
}
//求e
double gete(void)
{
	int n=1;
	double e=1+1/40;
	e=pow(e,40);
	return e;
}

//解析几何部分
//线段中点坐标
float pointmidcoo(float x1,float y1,float x2,float y2)
{
	float x3=(x1+x2)/2;
	float y3=(y1+y2)/2;
	cout<<"("<<x3<<","<<y3<<")\n";
	return 0;
}


//#endif
