#include<stdio.h>
#include<math.h>
#include"masterdata.h"
#include<iostream>

#define dct b*b-4*a*c
#define VA (4*a*c-b*b)/4*a


int conrol(void);
int ope(float a,float b,float c);

using namespace std;


int main()
{
	cout<<"MasterData for linux operation.\n";
	conrol();
	
	return 0;
}

int conrol(void)
{
	cout<<"二次函数因变量估算器，请输入运算次数.\n";
	cout<<"若干次输入自变量后将会退出程序.\n";
	int times;
	int i;
	cout<<">";//提示符
	cin>>times;
	float a,b,c;
	
	cout<<"请输入二次项，一次项的系数与常数项.\n";
	cin>>a>>b>>c;
	ope(a,b,c);
	if(a==0)
	{//变色显示
		cout<<"\033[45;34m 错误二次项系数不为0.\n\033[0m";
		whiteword();
		
	}
	else
	{
	//Nothing.
	}

	for(i=0;i<times;i++)
	{
	float y,x;//反复输入自变量
	cout<<"\033[0m";
	cout<<"请输入自变量.\n";
	cout<<">";
	cin>>x;
	y=a*x*x+b*x+c;//一般式
	cout<<"\n因变量为 "<<y<<endl;
		
	}

	return 0;
}

int ope(float a,float b,float c)
{
	if(a>0)
	{//看开口方向
	cout<<"该函数图像为一条开口向上的抛物线.\n";
	cout<<"因变量存在最小值"<<VA;
	cout<<"如果自变量小于"<<-1*b/(2*a)<<"时";
	cout<<"因变量随着自变量的增大而减小，反之增大"<<endl;
	}
	else
	{
	cout<<"该函数图像为一条开口向下的抛物线.\n";
	cout<<"因变量存在最大值 "<<VA;
	cout<<"如果自变量小于"<<-1*b/(2*a)<<"时";
	cout<<"因变量随着自变量的增大而增大，反之减小"<<endl;
	}
/*Points*/
	cout<<"对称轴为 x= "<<-1*b/(2*a);	
	cout<<"顶点为("<<-1*b/(2*a)<<","<<VA;
	cout<<"图像与Y轴交点为(0,"<<c<<")"<<endl;
	
	if(dct>0)//The situation about the root for the equation
	{
		cout<<"\033[1;36m对于方程 "<<a<<" x^2+ "<<b<<"x+"
		<<c<<" =0 有两个不等的实数根\n";
		double root1,root2;//实数根
		double(a);double(b);double(c);
		root1=(-1*b+sqrt(dct))/(2*a);
		root2=(-1*b-sqrt(dct))/(2*a);
		cout<<"两根大约为"<<root1<<"and"<<root2<<endl;
		cout<<"\033[1;33m 图像与X轴有两个交点 "<<endl;
	}else if(dct==0)
	{
		cout<<"\033[1;36m对于方程 "<<a<<" x^2+ "<<b<<"x+"
		<<c<<" =0 有两个相等的实数根\n";
		double root;
		double(a);double(b);double(c);
		root=(-1*b+sqrt(dct))/(2*a);
		cout<<"两根大约都为"<<root<<endl;
		cout<<"\033[1;33m 图像与X轴有一个交点 "<<endl;
	}else 
	{
		cout<<"\033[1;36m对于方程 "<<a<<" x^2+ "<<b<<"x+"
		<<c<<" =0 不存在实数根\n";
		cout<<"\033[1;33m 图像与X轴没有交点 \033[0m"<<endl;
	}
		
	return 0;
}
	

